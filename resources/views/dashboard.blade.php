<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dashboard | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
        <style>
        @media print
              {
                 .printhidden{
                   display:none !important;
                 }
              }
                .outer{
                  border : 1px solid #858785;
                    /* width : 100%; */
                    background-color: white;
                }
                 th, td {
                    border: 1px solid #f4f5f7;
                    /* padding: 9px; */
                    font-size: 9px;
                }
                td{
                  height: 35px;
                  width: 40px;
                }
                table {
                    border-spacing: 1px;
                }
                /* .fills{
                  background-color: #f9f9f9;
                  font-weight:bold;
                } */
                .textfill{
                  color: #000;
                  font-weight: bold;

                }
                .left{
                  float: left;
                }
                .fix{
                  height: 20px;
                }

                </style>
    </head>
    <body >
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar printhidden">
            <a class="c-navbar__brand" href="{{config('app.url')}}//dashboard">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>

           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="{{ route('meeseva.logout') }}"
                                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout
                                      <form id="logout-form" action="{{ route('meeseva.logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                      </a>
                </div>
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar u-mb-medium printhidden">
            <a href="{{ config('app.url') }}/dashboard"><h3 class="c-toolbar__title has-divider">Dashboard</h3></a>

            <a href="{{ config('app.url') }}/applications"><h3 class="c-toolbar__title has-divider">Report</h3></a>
            <a href="{{ config('app.url') }}/allapplications"><h3 class="c-toolbar__title has-divider">All Applications</h3></a>

            {{-- <h5 class="c-toolbar__meta u-mr-auto">Dashboard</h5> --}}



            <!-- Button trigger modal -->


            <!-- Modal -->

        </div><!-- // .c-toolbar -->

        <div class="container">
            @if(isset($data['applicationData']['error']))
                <h1>{{ $data['applicationData']['error'] }}</h1>
            @endif

          @if (isset($data['applicationData']) && !empty($data['applicationData']) && empty($data['applicationData']['error']))
          <div class="row">
              <div class="col-sm-6 col-lg-4">
                  <div class="c-state">
                      <h3 class="c-state__title">Total Applications</h3>
                      @php
                        setlocale(LC_MONETARY, 'en_IN');
                        $amount = $data['count'] * 400;
                      @endphp
                      <h4 class="c-state__number">{{$data['count']}} / {{moneyFormatIndia($amount)}}</h4>
                      {{-- <p class="c-state__status">13% Increase</p> --}}
                      <span class="c-state__indicator">
                          <i class="fa fa-arrow-circle-o-up"></i>
                      </span>
                  </div><!-- // c-state -->
              </div>
          </div><!-- // .row -->


            <h2>Approved</h2>
            <form class="" action="{{ route('paymentUpdate') }}" method="post">
              {{ csrf_field() }}
              <input type="hidden" name="updateId" value="{{ $data['applicationData']->first()->id }}">
              <input type="hidden" name="esevaId" value="{{ Auth::user()->id }}">
              <div class="row">
                <div class="col-sm-6 col-md-3 u-mb-small">
                    <div class="c-field">
                        <label class="c-field__label" for="input13">Name</label>
                        <input class="c-input" type="text" name="applicationNo" id="input13" value="{{ $data['applicationData']->first()->applicantName }}" placeholder="Clark" disabled>

                    </div>
                </div>

                <div class="col-sm-6 col-md-3 u-mb-small">
                    <div class="c-field">
                        <label class="c-field__label" for="input14">Village</label>
                        <input class="c-input" id="input14" name="adharNumber" type="text" value="{{ $data['applicationData']->first()->village }}" placeholder="Clark" disabled>

                    </div>
                </div>
                <div class="col-sm-6 col-md-3 u-mb-small">
                    <div class="c-field">
                        <label class="c-field__label" for="number">House No</label>
                        <input class="c-input" id="number" name="number" type="text" value="{{ $data['applicationData']->first()->houseNo }}" placeholder="number" disabled>

                    </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input13">Application Number</label>
                          <input class="c-input" type="text" name="applicationNo" id="input13" value="{{ $data['applicationData']->first()->applicationNo }}" placeholder="Clark" disabled>

                      </div>
                  </div>

                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input14">Aadhaar Number</label>
                          <input class="c-input" id="input14" name="adharNumber" type="text" value="{{ $data['applicationData']->first()->adharNumber }}" placeholder="Clark" disabled>

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="number">Mobile Number</label>
                          <input class="c-input" id="number" name="number" type="text" value="{{ $data['applicationData']->first()->mobileNo }}" placeholder="number" disabled>

                      </div>
                  </div>


              </div>  <!-- // .row -->
              <div class="row u-mb-large">
                <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-field">
                        <label class="c-field__label" for="input15">Payment Received</label>
                        <input class="c-input btn-warning" id="input15" type="submit" value="Payment Received">
                    </div>
                </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <a href="{{ config('app.url') }}/dashboard">
                    <div class="c-field">
                      <label class="c-field__label" for="input15">New</label>
                      <input class="c-input btn-warning" id="input15" type="button" value="New">
                    </div>
                  </a>
                  </div>
              </div>
              @isset($error)
                {{error}}
              @endisset
            </form>
          @else
            <form class="" action="{{ route('search') }}" method="post">
              {{ csrf_field() }}
              <div class="row u-mb-large printhidden">
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input13">Application Number</label>
                          <input class="c-input" type="text" name="applicationNo" id="input13" placeholder="Clark">

                      </div>
                  </div>

                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input14">Aadhaar Number</label>
                          <input class="c-input" id="input14" name="adharNumber" type="text" placeholder="Clark">

                      </div>
                  </div>

                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="mobileNo">Mobile Number</label>
                          <input class="c-input" id="mobileNo" name="mobileNo" type="text" placeholder="mobileNo">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input15">Search</label>
                          <input class="c-input btn-warning" id="input15" type="submit" value="Search">
                      </div>
                  </div>
              </div>  <!-- // .row -->
              @isset($error)
                {{error}}
              @endisset
            </form>
          @endif
        </div><!-- // .container -->
        @if (isset($data['printData']) && !empty($data['printData']))
          <div class="container ">
            <div class="col-xl-5">


                    <div class="outer">

                    <table style="Width:100%;">
                      <tr>
                        <td class="fills" width="70px !important">Application No :</td>
                        <td class="textfill">{{$data['printData']->applicationNo}}</td>
                        <td class="fills">Name :</td>
                        <td class="textfill">{{$data['printData']->applicantName}}</td>
                      </tr>
                       <tr>
                        <td class="fills">Service Name :</td>
                        <td class="textfill" width="150px;">Regularization of Unobjectionable Encroachments in Government Lands Up to 500 Sq.Yards(Go No:388)</td>
                        <td class="fills">Transaction Date :</td>
                        <td class="textfill">{{Date('Y/m/d')}} {{Date('H:s A')}}</td>
                      </tr>
                       <tr>
                        <td class="fills">Amount :</td>
                        <td class="textfill">400</td>
                        <td class="fills">Status :</td>
                        <td class="textfill">{{$data['printData']->status}}</td>
                      </tr>
                       <tr>
                        <td class="fills">Remarks :</td>
                        <td class="textfill">NA</td>
                        <td class="fills">Approved/Rejected Date :</td>
                        <td class="textfill">NA</td>
                      </tr>
                       <tr>
                        <td class="fills">Print Date :</td>
                        <td class="textfill">{{$data['printData']->updated_at}}</td>
                        <td class="fills">Printed By :</td>
                        <td class="textfill">{{Auth::user()->name}}</td>
                      </tr>
                       <tr>
                        <td class="fills">District :</td>
                        <td class="textfill">Chittoor</td>
                        <td class="fills">Mandal/Village :</td>
                        <td class="textfill">Tirupati Urban / {{$data['printData']->village}} </td>
                      </tr>
                    </table>
                    </div>
                    <br>
                    <div class="" style="padding-left:350px;">
                        <button type="button" name="Print" class="printhidden" onclick="myFunction()" style="background-color: #4CAF50; width:100px; height:40px; border-radius:10px; color:white;"> Print </button>
                    </div>
                    </div>
                    <br><br>
              </div>
        @endif


        <!-- Main javascsript -->
        {{-- <script type="text/javascript">
          Dropzone.options.imageUpload = {
            acceptedFiles: ".xlsx"
          };
        </script> --}}
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>

<script>
function myFunction() {
    window.print();
}
</script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
