@php
$flatDetails = $allotment;
// dd($allotment);
  // $exp = explode(',', $flatDetails);
  $table = 'aptalt';
  // if ($exp[0] == 'A') {
  //   $table = 'calc';
  // } elseif ($exp[0] == 'B') {
  //   $table = 'calcb';
  // } elseif ($exp[0] == 'C') {
  //   $table = 'calcc';
  // }
  $userDetails = DB::SELECT("SELECT * FROM `$table` WHERE id = '$allotment' LIMIT 1");
  // dd($userDetails);
@endphp
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>{{ $userDetails[0]->field1 }}</title>

    <style media="screen">
    body {
      font-family:sans-serif;
      font-weight: bold;
    }
      .head {
        margin-top: 20px;
      }
      .phead {
        font-family: sans-serif;
        font-weight: bold;
        margin-top: 25px;
      }
      .phead p {
        margin-bottom: 2px;
      }
    </style>
    <style media="print">
    body {
      font-family:sans-serif;
      font-weight: bold;
    }
    .head {
      margin-top: 20px;
    }
    .phead {
      font-family: sans-serif;
      font-weight: bold;
      margin-top: 25px;
    }
    .phead p {
      margin-bottom: 2px;
    }
    </style>
  </head>
  <body>
    <div class="container" style="border: 2px solid #000;">
  <div class="row" style="margin-top: 20px; margin-bottom: 20px;">
      <div class="col-sm-4 text-center">
        <img src="{{ config('app.url') }}/images/ap.png" alt="" style="width: 160px;">
      </div>
      <div class="col-sm-4 text-center phead">
        <p>PRADHAN MANTRI AWAS YOJANA</p>
        <p>NTR URBAN HOUSING</p>
        <p>PUNGANUR MUNICIPALITY</p>
        <p>CHITTOOR DISTRICT</p>
        <p>FLAT ALLOTMENT LETTER</p>
      </div>
      <div class="col-sm-4 text-center">
        <img src="{{ config('app.url') }}/images/aptidco.png" alt="" style="width: 160px;">
      </div>
  </div>

  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            I. Beneficiary Details
            <ul class="list-unstyled" style="margin-left: 25px;">

              <li>(1) Name</li>
              <li>(2) Father / Husband Name</li>
              <li>(3) Aadhaar Number</li>
            </ul>
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
          <br>
          <ul class="list-unstyled" style="margin-left: 25px;">

            <li>{{ $userDetails[0]->beneficiary_name }}</li>
            <li>{{ $userDetails[0]->husfather_name }}</li>
            <li>{{ $userDetails[0]->aadhar_no }}</li>
          </ul>
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            II.Address
            <ul class="list-unstyled" style="margin-left: 25px;">

              <li style="height: 45px;">(1) House Number</li>
              <li>(2) Ward Number</li>
            </ul>
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
          <br>
          <ul class="list-unstyled" style="margin-left: 25px;">
            {{-- @php
              $flatDetails = $userDetails[0]->allotment;
              $exp = explode(',', $flatDetails);
            @endphp --}}
            <li style="height: 45px;">{{ $userDetails[0]->house_no }}</li>
            <li>{{ $userDetails[0]->ward_no }}</li>
          </ul>
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            III. PMAY survey code number
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
          {{ '---------' }}
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            IV. Project details
            <ul class="list-unstyled" style="margin-left: 25px;">

              <li>(1) Project area</li>
              <li>(2) Project code</li>
              <li>(3) Home category</li>
            </ul>
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
          <br>
          <ul class="list-unstyled" style="margin-left: 25px;">

            <li>'---------'</li>
            <li>'---------'</li>
            <li>
              '---------'
              {{-- @if ($userDetails[0]->category == 1)
                300 Sft
              @elseif ($userDetails[0]->category == 2)
                365 Sft
              @elseif ($userDetails[0]->category == 3)
                430 Sft
              @endif --}}
            </li>
          </ul>
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            V. FLAT ALLOTMENT Details
            <ul class="list-unstyled" style="margin-left: 25px;">

              <li>(1) Block number</li>
              <li>(2) Survey Code</li>
              <li>(3) Allotment Number</li>
            </ul>
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
          <br>
          <ul class="list-unstyled" style="margin-left: 25px;">
            
            <li>{{ 'Block - A' }}</li>
            <li>
              @php
                  $str = $userDetails[0]->field1;
                  $explode = explode("-",$str);
              @endphp
              {{$explode[0]}}
          </li>
            <li>{{ $explode[1] }}</li>
          </ul>
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-6">

          <ul>
            VI. Beneficiary Bank Loan Amount
          </ul>

      </div>
      <div class="col-sm-6">
        <ul>
         '---------'
        </ul>
      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-12">

          <ul>
            * This allotment is only for Bank loan purpose
          </ul>

      </div>
  </div>
  <div class="row" style="margin-bottom: 20px;">
      <div class="col-sm-12">

          <ul class="text-right">
            <img src="{{ config('app.url') }}/images/signature.png" alt=""  style="width: 100px; margin-right: 115px;">
          </ul>
          <ul class="text-right" style="margin-right: 115px;">
            Commissioner
          </ul>
          <ul class="text-right" style="margin-right: 75px;">
            Tirupati municipality
          </ul>

      </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>
