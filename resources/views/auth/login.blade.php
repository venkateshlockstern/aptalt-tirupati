<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:38:40 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Log in | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body class="o-page o-page--center">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="o-page__card">
            <div class="c-card u-mb-xsmall">
                <header class="c-card__header u-pt-large">
                    <a class="c-card__icon" href="#!">
                        <img src="{{config('app.url')}}/img/logo-login.svg" alt="Dashboard UI Kit">
                    </a>
                    <h1 class="u-h3 u-text-center u-mb-zero">Welcome back! Please login.</h1>
                </header>

                <form class="c-card__body" action="{{ route('login') }}" method="post">
                  {{ csrf_field() }}
                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input1">Log in with your e-mail address</label>
                        <input id="email" type="email" class="c-input{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                        {{-- <input class="c-input" type="email" id="input1" placeholder="clark@dashboard.com"> --}}
                        @if ($errors->has('email'))
                          <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                          </span>
                        @endif
                    </div>



                    <div class="c-field u-mb-small">
                        <label class="c-field__label" for="input2">Password</label>
                        {{-- <input class="c-input" type="password" id="input2" placeholder="Numbers, Letters..."> --}}
                        <input id="password" type="password" class="c-input{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <button class="c-btn c-btn--info c-btn--fullwidth" type="submit">Sign in to Dashboard</button>
                </form>
            </div>

            {{-- <div class="o-line">
                <a class="u-text-mute u-text-small" href="register.html">Don’t have an account yet? Get Started</a>
                <a class="u-text-mute u-text-small" href="forgot-password.html">Forgot Password?</a>
            </div> --}}
        </div>

        <!-- Main javascsript -->
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:38:40 GMT -->
</html>
