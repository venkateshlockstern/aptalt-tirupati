<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:40 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Housing</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <div class="container">
            <div class="row">
              <div class="col-sm-3">
                <div class="u-text-center">
                  <br>
                    {{-- <img src="{{config('app.url')}}/cm1.png" width="130px" alt="Dashboard UI Kit" > --}}
                </div>
              </div>
                <div class="col-sm-6">
                  <br>
                    <div class="u-text-center">
                        <img src="{{config('app.url')}}/logo.png" alt="Dashboard UI Kit" >
                    </div>

                </div>
                <div class="col-sm-3">
                  <br>
                    {{-- <img src="{{config('app.url')}}/collector2.jpg" class="" width="130px" style="border-radius: 100px;" alt="Dashboard UI Kit" > --}}
                </div>
            </div>

            <div class="row">


                <div class="col-sm-12 col-lg-4">

                </div>
                <div class="col-sm-12 col-lg-4">
                    <div class="c-card u-p-medium u-text-center u-mb-medium" data-mh="landing-cards">

                        <img class="u-mb-small" src="{{ config('app.url') }}/apgovt.jpg" alt="iPhone icon">

                        <h4 class="u-h6 u-text-bold u-mb-small">
                          Commissioner
                        </h4>
                        <a class="c-btn c-btn--info" href="{{route('mroLogin')}}">Start using Dashboar</a>
                    </div>
                </div>


            </div>
            <div class="col-sm-12">
              <div class="u-mv-large u-text-center">
                <h2 class="u-mb-xsmall" style="color:red;">Help Desk: If Any Technical Issues Please Contact: 9700111444 </h2>
                <p class="u-mb-xsmall" style="color:red;">Designed and Developed by : <strong style="color:#1a91eb; font-weight:40px;">SHUKLA IT SOLUTIONS</strong>  </p>
              </div>
            </div>


        </div>

        <!-- Main javascsript -->
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
