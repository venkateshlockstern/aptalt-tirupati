<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>ApplicationUpdate | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body >
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar printhidden">
            <a class="c-navbar__brand" href="{{config('app.url')}}//dashboard">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>

           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="{{ route('meeseva.logout') }}"
                                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout
                                      <form id="logout-form" action="{{ route('meeseva.logout') }}" method="POST" style="display: none;">
                                          {{ csrf_field() }}
                                      </form>
                                      </a>
                </div>
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar u-mb-medium printhidden">
            <a href="{{ config('app.url') }}/mro"><h3 class="c-toolbar__title has-divider">Home</h3></a>

        </div><!-- // .c-toolbar -->

        <div class="container">

            <form class="" action="{{route('StatusUpdate')}}" method="post">
              {{ csrf_field() }}
              <div class="row u-mb-large printhidden">
                <input type="hidden" name="id" value="{{$key->id}}">
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input13">Application Number</label>
                          <input class="c-input" type="text" name="applicationNo" id="input13" value="{{$key->applicationNo}}" disabled>

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input13">Applicant Name</label>
                          <input class="c-input" type="text" name="applicantName" id="input13" value="{{$key->applicantName}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input14">Aadhaar Number</label>
                          <input class="c-input" id="input14" name="adharNumber" type="text" value="{{$key->adharNumber}}">

                      </div>
                  </div>

                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="mobileNo">Mobile Number</label>
                          <input class="c-input" id="mobileNo" name="mobileNo" type="text" value="{{$key->mobileNo}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="houseNo">Address</label>
                          <input class="c-input" id="houseNo" name="houseNo" type="text" value="{{$key->houseNo}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="village">Village</label>
                          <input class="c-input" id="village" name="village" type="text" value="{{$key->village}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="surveyNo">Survey NO</label>
                          <input class="c-input" id="surveyNo" name="surveyNo" type="text" value="{{$key->surveyNo}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="extents">Extents As Per Application</label>
                          <input class="c-input" id="extents" name="extentAsPerApplication" type="text" value="{{$key->extentAsPerApplication}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="TotalConfirmed">Confirmed Extents</label>
                          <input class="c-input" id="TotalConfirmed" name="extentTotalConfirmedAfterEnquiryOfSurveyor" type="text" value="{{$key->extentTotalConfirmedAfterEnquiryOfSurveyor}}">

                      </div>
                  </div>
                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="rationCardNumber">Ration Card Number</label>
                          <input class="c-input" id="rationCardNumber" name="rationCardNumber" type="text" value="{{$key->rationCardNumber}}">

                      </div>
                  </div>

                  <div class="col-sm-6 col-md-3 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="mobileNo">status</label>
                          {{-- <input class="c-input" id="mobileNo" name="mobileNo" type="text" value="{{$key->mobileNo}}"> --}}
                          <select class="c-select" name="sts">
                            <option @if ($key->sts == 1)
                              selected
                            @endif value="1">Active</option>
                            <option  @if ($key->sts == 0)
                              selected
                            @endif value="0">In-Active</option>
                          </select>
                      </div>
                  </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                      <div class="c-field">
                          <label class="c-field__label" for="input15">Update</label>
                          <input class="c-input btn-warning" id="input15" type="submit" value="Update">
                      </div>
                  </div>
              </div>  <!-- // .row -->

            </form>
        </div><!-- // .container -->

        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>

<script>
function myFunction() {
    window.print();
}
</script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
