<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dashboard | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body style="overflow-x: hidden;">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar">
            <a class="c-navbar__brand" href="{{config('app.url')}}//mro">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>

           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="{{ route('mro.logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">Logout
               <form id="logout-form" action="{{ route('mro.logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
             </a>
                </div>
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>
        <div class="c-toolbar u-mb-medium printhidden">
            <a href="{{ config('app.url') }}/dashboard"><h3 class="c-toolbar__title has-divider">Dashboard</h3></a>

            <a href="{{ config('app.url') }}/applications"><h3 class="c-toolbar__title has-divider">Report</h3></a>
            <a href="{{ config('app.url') }}/allapplications"><h3 class="c-toolbar__title has-divider">All Applications</h3></a>

            {{-- <h5 class="c-toolbar__meta u-mr-auto">Dashboard</h5> --}}



            <!-- Button trigger modal -->


            <!-- Modal -->

        </div><!-- // .c-toolbar -->


        <div class="col-md-12 u-mb-large">

        </div>
        <div class="container">


            <div class="row u-mb-large">
                <div class="col-12">
                    <div class="c-table-responsive@desktop">
                        <table class="c-table" id="datatable">
                            <caption class="c-table__title">
                                Payment Received Applications
                            </caption>

                            <thead class="c-table__head c-table__head--slim">
                                <tr class="c-table__row">
                                    <th class="c-table__cell c-table__cell--head no-sort">S.No</th>
                                    <th class="c-table__cell c-table__cell--head">Application No</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Applicant Name</th>
                                    <th class="c-table__cell c-table__cell--head">Phone</th>
                                    <th class="c-table__cell c-table__cell--head">Address</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Status</th>
                                </tr>
                            </thead>

                            <tbody>
                              @foreach ($data['applications'] as $application)
                                <tr class="c-table__row">
                                  <td class="c-table__cell">{{$application->sno}}</td>
                                  <td class="c-table__cell">{{$application->applicationNo}}</td>
                                  <td class="c-table__cell">{{$application->applicantName}}</td>
                                  <td class="c-table__cell">{{$application->mobileNo}}</td>
                                  <td class="c-table__cell"  data-toggle="tooltip" title="{{$application->houseNo}}">{{substr($application->houseNo,0,15)}}...</td>
                                  <td class="c-table__cell">
                                    @if ($application->sts == 0)
                                      In-Active
                                    @else
                                      @if ($application->payment_status == 1)
                                        Payment Recieved
                                      @else
                                        Pending
                                      @endif
                                    @endif
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  <!-- // .row -->
        </div><!-- // .container -->

        <!-- Main javascsript -->
        {{-- <script type="text/javascript">
          Dropzone.options.imageUpload = {
            acceptedFiles: ".xlsx"
          };
        </script> --}}
        <script type="text/javascript">
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
        <script>
        // $('.c-table').dataTable( {
        //   "searching": true
        // });
        </script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
