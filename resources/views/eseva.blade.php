<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dashboard | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body>
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar">
            <a class="c-navbar__brand" href="#!">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>

           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                {{-- <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>
                </div> --}}
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar u-mb-medium">
            <h3 class="c-toolbar__title has-divider">Home</h3>
            {{-- <h5 class="c-toolbar__meta u-mr-auto">Dashboard</h5> --}}



            <!-- Button trigger modal -->
            <button type="button" class="c-btn c-btn--success u-ml-small" data-toggle="modal" data-target="#modal8">
              Upload Excel
            </button>

            <!-- Modal -->
            <div class="col-sm-4 u-mb-medium">
                    <!-- Button trigger modal -->
                    {{-- <button type="button" class="c-btn c-btn--info" data-toggle="modal" data-target="#modal8">
                      Setup New Project
                    </button> --}}

                    <!-- Modal -->
                    <div class="c-modal c-modal--small modal fade" id="modal8" tabindex="-1" role="dialog" aria-labelledby="modal8" data-backdrop="static">
                        <div class="c-modal__dialog modal-dialog" role="document">
                            <div class="c-modal__content">

                                <div class="c-modal__header">
                                    <h3 class="c-modal__title">Upload Excell File</h3>

                                    <span class="c-modal__close" data-dismiss="modal" aria-label="Close">
                                        <i class="fa fa-close"></i>
                                    </span>
                                </div>

                                <div class="c-modal__body">
                                    <form action="{{route('mroUpload')}}" method="post" enctype="multipart/form-data" class="dropzone u-mb-small" id="modal-dropzone" style="height: 180px;">
                                      {{ csrf_field() }}
                                        <div class="dz-message" data-dz-message>
                                            <i class="dz-icon fa fa-cloud-upload"></i>
                                            <span>Drag a file here or browse for a file to upload.</span>
                                        </div>

                                        <div class="fallback">
                                            <input name="file" type="file" multiple>
                                        </div>
                                    </form>




                                </div>

                                <div class="c-modal__footer u-justify-center">
                                    <a class="c-btn c-btn--success" href="#" data-dismiss="modal">Close</a>
                                </div>

                            </div><!-- // .c-modal__content -->
                        </div><!-- // .c-modal__dialog -->
                    </div><!-- // .c-modal -->
                </div>
        </div><!-- // .c-toolbar -->

        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="c-state">
                        <h3 class="c-state__title">Total Applications</h3>
                        @php
                          setlocale(LC_MONETARY, 'en_IN');
                          $amount = $data['count'] * 400;
                        @endphp
                        <h4 class="c-state__number">{{$data['count']}} / {{moneyFormatIndia($amount)}}</h4>
                        {{-- <p class="c-state__status">13% Increase</p> --}}
                        <span class="c-state__indicator">
                            <i class="fa fa-arrow-circle-o-up"></i>
                        </span>
                    </div><!-- // c-state -->
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="c-state c-state--success">
                        <h3 class="c-state__title">Delivered Applications</h3>
                        <h4 class="c-state__number">5,990</h4>
                        {{-- <p class="c-state__status">4% Increase</p> --}}
                        <span class="c-state__indicator">
                            <i class="fa fa-arrow-circle-o-up"></i>
                        </span>
                    </div><!-- // c-state -->
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="c-state c-state--warning">
                        <h3 class="c-state__title">Pending Applications</h3>
                        <h4 class="c-state__number">$80,890</h4>
                        {{-- <p class="c-state__status">21% Decrease</p> --}}
                        <span class="c-state__indicator">
                            <i class="fa fa-arrow-circle-o-down"></i>
                        </span>
                    </div><!-- // c-state -->
                </div>
            </div><!-- // .row -->

            <div class="row u-mb-large">
                <div class="col-12">
                    <div class="c-table-responsive@desktop">
                        <table class="c-table" id="datatable">
                            <caption class="c-table__title">
                                Sortable Tables <small>Powered by DataTables</small>
                            </caption>

                            <thead class="c-table__head c-table__head--slim">
                                <tr class="c-table__row">
                                    <th class="c-table__cell c-table__cell--head no-sort">S.No</th>
                                    <th class="c-table__cell c-table__cell--head">Application No</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Applicant Name</th>
                                    <th class="c-table__cell c-table__cell--head">Phone</th>
                                    <th class="c-table__cell c-table__cell--head">Address</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Status</th>
                                </tr>
                            </thead>

                            <tbody>
                              @foreach ($data['applications'] as $application)
                                <tr class="c-table__row">
                                  <td class="c-table__cell">{{$application->sno}}</td>
                                  <td class="c-table__cell">{{$application->applicationNo}}</td>
                                  <td class="c-table__cell">{{$application->applicantName}}</td>
                                  <td class="c-table__cell">{{$application->mobileNo}}</td>
                                  <td class="c-table__cell">{{$application->houseNo}}</td>
                                  <td class="c-table__cell">Finishing</td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  <!-- // .row -->
        </div><!-- // .container -->

        <!-- Main javascsript -->
        {{-- <script type="text/javascript">
          Dropzone.options.imageUpload = {
            acceptedFiles: ".xlsx"
          };
        </script> --}}
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
