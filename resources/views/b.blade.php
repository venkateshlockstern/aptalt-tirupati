<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Lucky Draw Allotments</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body style="overflow-x: hidden;">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar">
            <a class="c-navbar__brand" href="{{config('app.url')}}//mro">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>
            <a class="" href="{{config('app.url')}}/assign">
                Upload Excell
            </a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="" href="{{config('app.url')}}/block/a">
                Report
            </a>

           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="{{ route('mro.logout') }}" onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">Logout
               <form id="logout-form" action="{{ route('mro.logout') }}" method="POST" style="display: none;">
                   {{ csrf_field() }}
               </form>
             </a>
                </div>
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar">
            {{-- <h5 class="c-toolbar__meta u-mr-auto">Dashboard</h5> --}}
            <div class="col-md-12">

              <nav class="nav">
                <a class="nav-link active" href="{{ config('app.url') }}/block/a" style="margin-right: 25px;">Report</a>
                
                
                {{-- <a class="nav-link disabled" href="#">Disabled</a> --}}
              </nav>

            </div>


            <!-- Button trigger modal -->


            <!-- Modal -->
            <div class="col-sm-4 u-mb-medium">

                    <!-- Button trigger modal -->
                    {{-- <button type="button" class="c-btn c-btn--info" data-toggle="modal" data-target="#modal8">
                      Setup New Project
                    </button> --}}

                    <!-- Modal -->

                </div>
        </div><!-- // .c-toolbar -->
        <div class="col-md-12 u-mb-large">

        </div>
        <div class="container">
            <div class="row">



            </div><!-- // .row -->

            <div class="row u-mb-large">
                <div class="col-12">
                    <div class="c-table-responsive@desktop">
                        <table class="c-table" id="datatable">
                            <caption class="c-table__title">
                                Sortable Tables <small>Powered by DataTables</small>
                            </caption>

                            <thead class="c-table__head c-table__head--slim">
                                <tr class="c-table__row">
                                    <th class="c-table__cell c-table__cell--head no-sort">S.No</th>
                                    <th class="c-table__cell c-table__cell--head">Name</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Father Name</th>
                                    <th class="c-table__cell c-table__cell--head">Phone</th>
                                    <th class="c-table__cell c-table__cell--head">Aadhaar</th>
                                    <th class="c-table__cell c-table__cell--head">Dob</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">ward Name</th>
                                    <th class="c-table__cell c-table__cell--head no-sort">Survey Code</th>
                                    <th class="c-table__cell c-table__cell--head">allotment</th>
                                </tr>
                            </thead>
                            <tbody>
                              @php
                                $list = DB::SELECT("SELECT * FROM `calcb` ORDER BY `days` DESC");
                                // dd($list);
                                $sno = 1;
                              @endphp
                              @foreach ($list as $key)
                                <tr>
                                  <td class="c-table__cell">{{ $sno++ }}</td>
                                  <td class="c-table__cell">{{ $key->name }}</td>
                                  <td class="c-table__cell">{{ $key->fname }}</td>
                                  <td class="c-table__cell">{{ $key->mobile }}</td>
                                  <td class="c-table__cell">{{ $key->aadhaar }}</td>
                                  {{-- @php
                                    $dob = Carbon\Carbon::createFromFormat('Y-m-d', $key->dob)->format('d-m-Y');
                                  @endphp --}}
                                  <td class="c-table__cell">{{ $key->days }}</td>
                                  <td class="c-table__cell">{{ $key->wardname }}</td>
                                  <td class="c-table__cell">{{ $key->scode }}</td>
                                  <td class="c-table__cell"><a href="{{ config('app.url') }}/pdf/{{ $key->allotment }}" target="_blank">{{ $key->allotment }}</a></td>
                                </tr>
                              @endforeach
                            </tbody>


                        </table>
                    </div>
                </div>
            </div>  <!-- // .row -->
        </div><!-- // .container -->

        <!-- Main javascsript -->
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>


        {{-- <script type="text/javascript">
          Dropzone.options.imageUpload = {
            acceptedFiles: ".xlsx"
          };
        </script> --}}
        <script>
        $(document).ready(function(){
          $('[data-toggle="tooltip"]').tooltip();
        });
        </script>
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
        {{-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> --}}
        <script src="https://nightly.datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="{{ config('app.url') }}/js/dataTables.buttons.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
