<!doctype html>
<html lang="en-us">

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
<head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Dashboard | Dashboard UI Kit</title>
        <meta name="description" content="Dashboard UI Kit">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">


        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600" rel="stylesheet">

        <!-- Favicon -->
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">

        <!-- Main Stylesheet -->
        <link rel="stylesheet" href="{{config('app.url')}}/css/main.min3661.css?v=2.0">
    </head>
    <body style="overflow-x: hidden;">
        <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

        <header class="c-navbar">
            <a class="c-navbar__brand" href="{{config('app.url')}}/mro">
                <img src="{{config('app.url')}}/img/logo.png" alt="Dashboard UI Kit">
            </a>
            <a class="" href="{{config('app.url')}}/mro">
                Home
            </a>


           <!-- Navigation items that will be collapes and toggle in small viewports -->

            <!-- // Navigation items  -->

            <div class="c-dropdown u-ml-auto dropdown">
                <a  class="c-avatar c-avatar--xsmall " href="#" id="dropdwonMenuAvatar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{Auth::user()->name}}
                </a>

                {{-- <div class="c-dropdown__menu dropdown-menu dropdown-menu-right" aria-labelledby="dropdwonMenuAvatar">
                    <a class="c-dropdown__item dropdown-item" href="#">Edit Profile</a>
                    <a class="c-dropdown__item dropdown-item" href="#">View Activity</a>
                    <a class="c-dropdown__item dropdown-item" href="#">Manage Roles</a>
                </div> --}}
            </div>

            {{-- <button class="c-nav-toggle" type="button" data-toggle="collapse" data-target="#main-nav">
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
                <span class="c-nav-toggle__bar"></span>
            </button> --}}
            <!-- // .c-nav-toggle -->
        </header>

        <div class="c-toolbar">
            {{-- <h5 class="c-toolbar__meta u-mr-auto">Dashboard</h5> --}}



            <!-- Button trigger modal -->
            <div class="col-md-12">
              <form class="c-form" action="{{ route('esevaSave') }}" method="post" style="width: 100%;">
                {{ csrf_field() }}
                <div class="row">
                  {{-- <h3 class="">Dashboard</h3> --}}

                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-form-field">
                      {{-- <date-util format="dd/MM/yyyy"></date-util> --}}

                      <label class="c-field__label" for="input14">Name</label>
                      <input class="c-input" id="input14" name="name" type="text" placeholder="Clark">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-form-field">
                      {{-- <date-util format="dd/MM/yyyy"></date-util> --}}

                      <label class="c-field__label" for="input14">Email</label>
                      <input class="c-input" id="input14" name="email" type="email" placeholder="Clark@clark.com">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-form-field">
                      <label class="c-field__label" for="input15">Password</label>
                      <input class="c-input" id="input15" name="password" type="password" placeholder="Clark">
                    </div>
                  </div>
                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-form-field">
                      <label class="c-field__label" for="input15">Password Confirm</label>
                      <input class="c-input" id="input15" name="password_confirm" type="password" placeholder="Clark">
                    </div>
                  </div>

                  <div class="col-sm-6 col-md-2 u-mb-small">
                    <div class="c-form-field">
                      <label class="c-field__label" for="input15">Create User</label>
                      <input class="c-input btn-warning" id="input15" type="submit" value="Create User">
                    </div>
                  </div>
                </div>

              </form>
            </div>

            <!-- Modal -->

        </div><!-- // .c-toolbar -->
        <div class="col-md-12 u-mb-large">

        </div>
        <div class="container">


            <div class="row u-mb-large">
                <div class="col-12">
                    <div class="c-table-responsive@desktop">
                        <table class="c-table" id="datatable">
                            <caption class="c-table__title">
                                Eseva's List
                            </caption>

                            <thead class="c-table__head c-table__head--slim">
                                <tr class="c-table__row">
                                    <th class="c-table__cell c-table__cell--head no-sort">Name</th>
                                    <th class="c-table__cell c-table__cell--head">E@Mial</th>
                                    <th class="c-table__cell c-table__cell--head">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                              @foreach ($data['esevaList'] as $eseva)
                                <tr class="c-table__row">
                                  <td class="c-table__cell">{{$eseva->name}}</td>
                                  <td class="c-table__cell">{{$eseva->email}}</td>
                                  <td class="c-table__cell">


                                      <a href="{{ config('app.url') }}/mro/eseva/update/{{ $eseva->id }}">Edit</a>
                                  </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  <!-- // .row -->
        </div><!-- // .container -->

        <!-- Main javascsript -->
        {{-- <script type="text/javascript">
          Dropzone.options.imageUpload = {
            acceptedFiles: ".xlsx"
          };
        </script> --}}
        <script src="{{config('app.url')}}/js/main.min3661.js?v=2.0"></script>
    </body>

<!-- Mirrored from zawiastudio.com/dashboard/demo/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 08 Feb 2018 18:37:43 GMT -->
</html>
