<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@indexa')->name('home');
Route::get('/applications', 'HomeController@applications')->name('applications');
Route::get('/allapplications', 'HomeController@allapplications');
// Route::get('/eseva/seach', 'HomeController@search')->name('search');
Route::post('/eseva/seach', 'HomeController@search')->name('search');
Route::post('/eseva/paymentUpdate', 'HomeController@paymentUpdate')->name('paymentUpdate');
Route::post('/meeseva/logout', 'Auth\LoginController@MeesevaLogout')->name('meeseva.logout');

Route::prefix('assign')->group(function() {
  Route::get('/login', 'Auth\MroLoginController@loginForm')->name('mroLogin');
  Route::post('/login', 'Auth\MroLoginController@login')->name('mroLogin');
  Route::post('/upload', 'MroController@upload')->name('mroUpload');
  Route::get('/excelUpload', 'MroController@excelUploadForm');
  Route::post('/excelUpload', 'MroController@excelUploadNew')->name('excelUpload');
  Route::post('/excelUploadApt', 'MroController@excelUploadApt')->name('excelUploadApt');
  Route::post('/search', 'MroController@search')->name('mroSearch');
  Route::get('/eseva', 'MroController@eseva')->name('eseva');
  Route::post('/eseva/save', 'MroController@esevaSave')->name('esevaSave');
  Route::get('/eseva/update/{id}', 'MroController@esevaUpdateForm')->name('esevaUpdateForm');
  Route::post('/eseva/update', 'MroController@esevaEdit')->name('esevaUpdate');
  Route::get('/ApplicationUpdate/{id}', 'MroController@ApplicationUpdate')->name('ApplicationUpdate');
  Route::post('/StatusUpdate', 'MroController@StatusUpdate')->name('StatusUpdate');
  // Route::post('dropzone/store', ['as'=>'dropzone.store','uses'=>'HomeController@dropzoneStore']);
  Route::get('/', 'MroController@index')->name('MroDashboard');
  Route::post('/logout', 'Auth\MroLoginController@MroLogout')->name('mro.logout');
  Route::post('/generate', 'MroController@generate')->name('generate');
  Route::post('/generateb', 'MroController@generateb')->name('generateb');
  Route::post('/generatec', 'MroController@generatec')->name('generatec');
});

Route::get('/block/success', 'MroController@success')->name('MroDashboardsuccess');
Route::get('/block/a', 'MroController@indexa')->name('MroDashboarda');
Route::get('/block/b', 'MroController@indexb')->name('MroDashboardb');
Route::get('/block/c', 'MroController@indexc')->name('MroDashboardc');
Route::post('/sms/a', 'MroController@smsa')->name('smsa');
Route::post('/sms/b', 'MroController@smsb')->name('smsb');
Route::post('/sms/c', 'MroController@smsc')->name('smsc');

Route::get('/pdf/{allotment}', 'MroController@pdf')->name('pdf');
