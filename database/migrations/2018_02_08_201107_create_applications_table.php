<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sno')->nullable();
            $table->string('applicationNo')->nullable();
            $table->string('applicantName')->nullable();
            $table->string('houseNo')->nullable();
            $table->string('mobileNo')->nullable();
            $table->string('surveyNo')->nullable();
            $table->string('village')->nullable();
            $table->string('extentAsPerApplication')->nullable();
            $table->string('extentTotalConfirmedAfterEnquiryOfSurveyor')->nullable();
            $table->string('rationCardNumber')->nullable();
            $table->string('adharNumber')->nullable();
            $table->string('status')->default('Approved');
            $table->string('payment_status')->nullable();
            $table->string('user_id')->nullable();
            $table->string('sts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
