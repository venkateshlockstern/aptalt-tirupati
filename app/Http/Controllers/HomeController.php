<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applicationsCount = Application::where('user_id', Auth::user()->id);
        $data = [
          'count' => $applicationsCount->count(),
          'applications' => $applicationsCount
        ];
        return view('dashboard')->with('data', $data);
    }

    public function applications()
    {
      $user = Auth::user();
      $user_id = $user->id;
      // return $user_id;
      $applications = Application::where('user_id', $user_id)->get();
      // return var_dump($finishedCount);
      // return $applications;
      $data = [
        'applications' => $applications
      ];
      return view('eesevaApplications')->with('data', $data);
    }

    public function allapplications()
    {
      $user = Auth::user();
      // $user_id = $user->id;
      // return $user_id;
      $applications = Application::all();
      // return var_dump($finishedCount);
      // return $applications;
      $data = [
        'applications' => $applications
      ];
      return view('eesevaApplications')->with('data', $data);
    }

    public function search(Request $request)
    {
      if ($request->applicationNo != '') {
        $column = "applicationNo";
        $searchValue = $request->applicationNo;
        // return 1;
      } elseif ($request->adharNumber != '') {
        $column = "adharNumber";
        $searchValue = $request->adharNumber;
        // return 2;
      } elseif ($request->mobileNo != '') {
        $column = "mobileNo";
        $searchValue = $request->mobileNo;
      } else {
        // return '3';
        return redirect()->back()->with('error', 'please fill one of the fields');
      }

      $getApplication = Application::where($column, $searchValue)->where('sts', '1')->get();
      if ($getApplication->count()) {
        $applications = $getApplication;
      } else {
        $applications = [
          'error' => 'Not Found'
        ];
      }
      // return $getApplication;
      $user_id = Auth::user()->id;
      $applicationsCount = Application::where('user_id', $user_id)->get();
      $data = [
        'count' => $applicationsCount->count(),
        'applications' => $applicationsCount,
        'applicationData' => $applications
      ];
      return view('dashboard')->with('data', $data);
    }

    public function paymentUpdate(Request $request)
    {
      $updateApplication = Application::find($request->updateId);
      $updateApplication->payment_status = 1;
      $updateApplication->user_id = $request->esevaId;
      $updateApplication->save();
      $data = [
          'printData' => $updateApplication,
      ];
      // return $updateApplication;
      return view('dashboard')->with('data', $data);
    }

}
