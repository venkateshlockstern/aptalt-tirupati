<?php

namespace App\Http\Controllers;

use App\ExcellUpload;
use Illuminate\Http\Request;

class ExcellUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\excellUpload  $excellUpload
     * @return \Illuminate\Http\Response
     */
    public function show(ExcellUpload $ExcellUpload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\excellUpload  $excellUpload
     * @return \Illuminate\Http\Response
     */
    public function edit(ExcellUpload $ExcellUpload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\excellUpload  $excellUpload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExcellUpload $ExcellUpload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\excellUpload  $excellUpload
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExcellUpload $ExcellUpload)
    {
        //
    }
}
