<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class MroLoginController extends Controller
{
    public function __construct()
    {
      $this->middleware('guest:admin', ['except'=>['MroLogout']]);
    }

    public function loginForm()
    {
      return view('auth.mroLogin');
    }

    public function login(Request $request)
    {

      $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required|min:6'
      ]);

      if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        return redirect()->intended(route('MroDashboard'));
      }
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    public function MroLogout()
    {
        Auth::guard('admin')->logout();
        return redirect('/');
    }

}
