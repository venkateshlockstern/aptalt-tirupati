<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExcellUpload;
use App\User;
use App\Application;
use Excel;
use Carbon;
use Hash;
use DB;

class MroController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $applicationsCount = Application::all();
        $finishedCount = Application::whereNotNull('payment_status')->get();
        $pendingCount = Application::whereNull('payment_status')->get();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'count' => $applicationsCount->count(),
          'finishedCount' => $finishedCount->count(),
          'pendingCount' => $pendingCount->count(),
          'applications' => $applicationsCount,
          'finishedApplications' => $finishedCount,
          'pendingApplications' => $pendingCount,
          'esevaList' => $esevaList
        ];
        return view('mro')->with('data', $data);
    }
    public function success()
    {
        
        return view('success');
    }
    public function indexa()
    {
        $applicationsCount = Application::all();
        $finishedCount = Application::whereNotNull('payment_status')->get();
        $pendingCount = Application::whereNull('payment_status')->get();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'count' => $applicationsCount->count(),
          'finishedCount' => $finishedCount->count(),
          'pendingCount' => $pendingCount->count(),
          'applications' => $applicationsCount,
          'finishedApplications' => $finishedCount,
          'pendingApplications' => $pendingCount,
          'esevaList' => $esevaList
        ];
        return view('a')->with('data', $data);
    }
    public function indexb()
    {
        $applicationsCount = Application::all();
        $finishedCount = Application::whereNotNull('payment_status')->get();
        $pendingCount = Application::whereNull('payment_status')->get();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'count' => $applicationsCount->count(),
          'finishedCount' => $finishedCount->count(),
          'pendingCount' => $pendingCount->count(),
          'applications' => $applicationsCount,
          'finishedApplications' => $finishedCount,
          'pendingApplications' => $pendingCount,
          'esevaList' => $esevaList
        ];
        return view('b')->with('data', $data);
    }
    public function indexc()
    {
        $applicationsCount = Application::all();
        $finishedCount = Application::whereNotNull('payment_status')->get();
        $pendingCount = Application::whereNull('payment_status')->get();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'count' => $applicationsCount->count(),
          'finishedCount' => $finishedCount->count(),
          'pendingCount' => $pendingCount->count(),
          'applications' => $applicationsCount,
          'finishedApplications' => $finishedCount,
          'pendingApplications' => $pendingCount,
          'esevaList' => $esevaList
        ];
        return view('c')->with('data', $data);
    }

    public function smsa()
    {
      $sms = DB::SELECT("SELECT * FROM calc"); // Ground Floor
      foreach ($sms as $s) {
        if ($s->mobile != '') {

          $flatDetails = $s->allotment;
          $exp = explode(',', $flatDetails);
          $blockno = $exp[0].$exp[1];
          $flatno = $exp[2].$exp[3];
          $mobile = $s->mobile;
          $message = "Dear $s->name of the application you have been allotted block no $blockno. flat no $flatno floor in pmay ntr urban housing in punganur Municipality ...  By commissioner punganur municipality";
          $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=COMPGR&password=abc@123&to={$mobile}&from=COMPGR&message=".urlencode($message);
          file($url);
        }

      }
      return redirect('/assign');
    }
    public function smsb()
    {
      $sms = DB::SELECT("SELECT * FROM calcb"); // Ground Floor
      foreach ($sms as $s) {
        if ($s->mobile != '') {

          $flatDetails = $s->allotment;
          $exp = explode(',', $flatDetails);
          $blockno = $exp[0].$exp[1];
          $flatno = $exp[2].$exp[3];
          $mobile = $s->mobile;
          $message = "Dear $s->name of the application you have been allotted block no $blockno. flat no $flatno floor in pmay ntr urban housing in punganur Municipality ...  By commissioner punganur municipality";
          $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=COMPGR&password=abc@123&to={$mobile}&from=COMPGR&message=".urlencode($message);
          file($url);
        }

      }
      return redirect('/assign');
    }
    public function smsc()
    {
      $sms = DB::SELECT("SELECT * FROM calcc"); // Ground Floor
      foreach ($sms as $s) {
        if ($s->mobile != '') {

          $flatDetails = $s->allotment;
          $exp = explode(',', $flatDetails);
          $blockno = $exp[0].$exp[1];
          $flatno = $exp[2].$exp[3];
          $mobile = $s->mobile;
          $message = "Dear $s->name of the application you have been allotted block no $blockno. flat no $flatno floor in pmay ntr urban housing in punganur Municipality ...  By commissioner punganur municipality";
          $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=COMPGR&password=abc@123&to={$mobile}&from=COMPGR&message=".urlencode($message);
          file($url);
        }

      }
      return redirect('/assign');
    }
    public function eseva()
    {

        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'esevaList' => $esevaList
        ];
        return view('mroEseva')->with('data', $data);
    }

    public function esevaUpdateForm($id)
    {

        $esevaList = User::find($id);
        // return $esevaList;
        return view('mroEsevaUpdateForm')->with('data', $esevaList);
    }

    public function esevaSave(Request $request)
    {

        $newEseva = new User();
        $newEseva->name = $request->name;
        $newEseva->email = $request->email;
        $newEseva->password = Hash::make($request->password);
        // return Hash::make($request->password);
        $newEseva->save();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'esevaList' => $esevaList
        ];
        return view('mroEseva')->with('data', $data);
    }

    public function esevaEdit(Request $request)
    {

        $editEseva = User::find($request->id);
        $editEseva->name = $request->name;
        $editEseva->email = $request->email;
        $editEseva->password = Hash::make($request->password);
        // return Hash::make($request->password);
        $editEseva->save();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'esevaList' => $esevaList
        ];
        return view('mroEseva')->with('data', $data);
    }

    public function search(Request $request)
    {
      // return $request;
        // if (!empty($request->esava) && !empty($request->from) && !empty(!empty($request->to))) {
        //   $date = date('Y-m-d', strtotime($request->from));
        //   $applicationsCount = Application::where('payment_status', 1)->where('user_id', $request->esava)->whereRaw('DATE(updated_at) = ?', [$date])->get();
        // }
        if (empty($request->esava) && empty($request->from) && empty($request->to) && empty($request->payment)) {
          return redirect('/mro');
        }

        if (!empty($request->esava)) {
          $applicationsCount = Application::where('payment_status', 1)->where('user_id', $request->esava);
          $finishedCount = Application::where('payment_status', 1)->where('user_id', $request->esava);
        }

        if (!empty($request->from) && empty($request->to)) {
          $date = date('Y-m-d', strtotime($request->from));
          $toDate = date('Y-m-d');
          // return $date;
          $dayStart =  Carbon::parse($date)->startOfDay();
          $dayEnd =  Carbon::parse($toDate)->endOfDay();
          $applicationsCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
          $finishedCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
          // return $applicationsCount->get();
        }

        if (empty($request->from) && !empty($request->to)) {
          $date = date('Y-m-d', strtotime('-3 days'));
          $toDate = date('Y-m-d', strtotime($request->to));
          // $dateS = new Carbon('first day of January 2016');
          // return $dateS;
          $dayStart =  Carbon::parse($date)->startOfDay();
          $dayEnd =  Carbon::parse($toDate)->endOfDay();
          $applicationsCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
          $finishedCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
        }

        if (!empty($request->from) && !empty($request->to)) {
          $date = date('Y-m-d', strtotime($request->from));
          $toDate = date('Y-m-d', strtotime($request->to));
          // $dateS = new Carbon('first day of January 2016');
          // return $dateS;
          $dayStart =  Carbon::parse($date)->startOfDay();
          $dayEnd =  Carbon::parse($toDate)->endOfDay();
          $applicationsCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
          $finishedCount->whereBetween('updated_at', [$dayStart, $dayEnd]);
        }

        if (!empty($request->payment)) {
          $applicationsCount = Application::where('payment_status', 1);
          $finishedCount = Application::where('payment_status', 1);
        }

        $appCount = $applicationsCount->get();
        $finishedAppCount = $applicationsCount->get();

        // return $appCount;
        $pendingCount = Application::whereNull('payment_status')->get();
        $esevaList = User::all();
        // return var_dump($finishedCount);
        $data = [
          'count' => $appCount->count(),
          'finishedCount' => $finishedAppCount->count(),
          'pendingCount' => $pendingCount->count(),
          'applications' => $appCount,
          'finishedApplications' => $finishedAppCount,
          'pendingApplications' => $pendingCount,
          'esevaList' => $esevaList
        ];
        return view('mro')->with('data', $data);
    }

    public function upload(Request $request)
    {
      $image = $request->file('file');
      $imageName = time().'-'.$image->getClientOriginalName();
      $image->move(public_path('excells'),$imageName);
      $saveExcell = new ExcellUpload();
      $saveExcell->excell = $imageName;
      $saveExcell->save();
      $message = 'Mro Uploaded new Excell';
      $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=vjsoft&password=vj@123&to=7013089044,9490501349,9700111444,9640195798&from=vJsOFT&message=".urlencode($message);
      file($url);
      return response()->json(['success'=>$imageName]);


    }

    public function excelUploadForm()
    {
      return view('upload');
    }



    public function excelUpload(Request $request)
    {
      // config(['excel.import.dates.columns' => [
      //       'd.o.b'
      //   ]]);
      // Inset Excel Data to Database
      $path = $request->file('file')->getRealPath();
      $data[] = \Excel::load($path)->setDateFormat(false)->get();

      // return $data;

      if ($data) {
        $category = $request['category'];
        $table = '';
        if ($category == 1) {
          $table = 'calc';
        } elseif ($category == 2) {
          $table = 'calcb';
        } elseif ($category == 3) {
          $table = 'calcc';
        }
        $delete = DB::DELETE("truncate `$table`");
        foreach ($data as $val) {
          // return $val;
          foreach ($val as $value) {
            // return $value[0];
            // $dob = $value['d.o.b'];
            // $dob = Carbon\Carbon::createFromFormat('d/m/Y', $dob)->format('Y-m-d');
            // // return $dob;
            // $dat = substr($dob, 0, 10);
            // $earlier = new \DateTime($dat);
            // $later = new \DateTime(Date('Y-m-d'));
            // $days = $later->diff($earlier)->format("%a");
            // return $days;
            // echo "<pre>";


            // foreach($value as $v) {
            $name = $value['name'];
            $fname = $value['father_name'];
            $scode = $value['scode'];
            $address = $value['address'];
            $diability = $value['disabilityyesno'];
            $aadhaar = $value['aadhar_no'];
            // $dob = $dat;
            $cDays = $value['days'];
            if ($diability != '') {
              $cDays = 1000;
            }
            $days = $cDays;
            // $pmay = $value['pmay_survey_code'];
            $mobile = $value['mobile_numnber'];
            $wardname = $value['ward_name'];
            $allotment = null;


            $insert = DB::INSERT("INSERT INTO `$table`
              (category, name, fname, diability, aadhaar, days, mobile, wardname, scode, address)
              VALUES
              ('$category', '$name', '$fname', '$diability', '$aadhaar', '$days', '$mobile', '$wardname', '$scode', '$address')
              ");
            // }
          }
          return redirect('/assign');
        }
      }
    }

    public function excelUploadNew(Request $request)
    {
      // return $request;
      // config(['excel.import.dates.columns' => [
      //       'd.o.b'
      //   ]]);
      // Inset Excel Data to Database
      $path = $request->file('file')->getRealPath();
      $data[] = \Excel::load($path)->setDateFormat(false)->get();

      // return $data;

      if ($data) {
        $category = 3;
        $table = 'aptalt';
        // if ($category == 1) {
        //   $table = 'calc';
        // } elseif ($category == 2) {
        //   $table = 'calcb';
        // } elseif ($category == 3) {
        //   $table = 'calcc';
        // }
        $delete = DB::DELETE("truncate `$table`");
        foreach ($data as $val) {
          // return $val;
          foreach ($val as $value) {
          $application_no = $value['application_no'];
          $beneficiary_name = $value['beneficiary_name'];
          $husfather_name = $value['husfather_name'];
          $aadhar_no = $value['aadhar_no'];
          $aadhar2 = $value['aadhar2'];
          $age = $value['age'];
          $house_no = $value['house_no'];
          $street_no = $value['street_no'];
          $ward_no = $value['ward_no'];
          $mobile_no = $value['mobile_no'];
          $status = $value['status'];
          $cd = $value['cd'];


            $insert = DB::INSERT("INSERT INTO `$table`
              (application_no, beneficiary_name, husfather_name, aadhar_no, aadhar2, age, house_no, street_no, ward_no, mobile_no, status, cd)
              VALUES
              ('$application_no', '$beneficiary_name', '$husfather_name', '$aadhar_no', '$aadhar2', '$age', '$house_no', '$street_no', '$ward_no', '$mobile_no', '$status', '$cd')
              ");
            // }
          }
          return redirect('/block/a');
        }
      }
    }

    public function excelUploadApt(Request $request)
    {
      // return $request;
      // config(['excel.import.dates.columns' => [
      //       'd.o.b'
      //   ]]);
      // Inset Excel Data to Database
      $path = $request->file('file')->getRealPath();
      $data[] = \Excel::load($path)->setDateFormat(false)->get();

      // return $data;

      if ($data) {
        $category = 3;
        $table = 'layouts';
        // if ($category == 1) {
        //   $table = 'calc';
        // } elseif ($category == 2) {
        //   $table = 'calcb';
        // } elseif ($category == 3) {
        //   $table = 'calcc';
        // }
        $delete = DB::DELETE("truncate `$table`");
        foreach ($data as $val) {
        // return $val;
        $sy_nosd_no_old = '';
        $name_of_the_district_old = '';
        $name_of_the_mandal_old = '';
        $name_of_the_village_old = '';
        $allotted_block_old = '';
          foreach ($val as $value) {
            if($value['sy_nosd_no']) {
            $sy_nosd_no_old = $value['sy_nosd_no'];
            }
            if($value['name_of_the_district']) {
            $name_of_the_district_old = $value['name_of_the_district'];
            }
            if($value['name_of_the_mandal']) {
            $name_of_the_mandal_old = $value['name_of_the_mandal'];
            }
            if($value['name_of_the_village']) {
            $name_of_the_village_old = $value['name_of_the_village'];
            }
            if($value['allotted_block']) {
            $allotted_block_old = $value['allotted_block'];
            }
          $sy_nosd_no = $sy_nosd_no_old;
          $name_of_the_district = $name_of_the_district_old;
          $name_of_the_mandal = $name_of_the_mandal_old;
          $name_of_the_village = $name_of_the_village_old;
          $allotted_block = $allotted_block_old;
          $alotted_plot = $value['alotted_plot'];
          $plot = $sy_nosd_no_old.'-'.$value['alotted_plot'];

            if($alotted_plot) {
            $insert = DB::INSERT("INSERT INTO `$table`
              (sy_nosd_no, alotted_plot, plot, name_of_the_district, name_of_the_mandal, name_of_the_village, allotted_block)
              VALUES
              ('$sy_nosd_no', '$alotted_plot', '$plot', '$name_of_the_district', '$name_of_the_mandal', '$name_of_the_village', '$allotted_block')
              ");
            }
            
            // }
          }
          return redirect('/block/a');
        }
      }
    }



    // Genarate A Block
    public function generate(Request $request)
    {
      // return $request;
    $countLayouts = DB::SELECT("SELECT count(*) as count FROM layouts");
    $layoutCount = $countLayouts[0]->count;
    $layoutCountC = $countLayouts[0]->count + 1;
    $list = DB::SELECT("SELECT * FROM aptalt");
    // return $countLayouts[0]->count;
    $randLayouts = DB::SELECT("SELECT * FROM layouts ORDER BY RAND()");
    for ($i=0; $i < $layoutCount; $i++) { 
      $plotAssign = $randLayouts[$i]->plot;
      // return $plotAssign;
      $plotAssignId = $randLayouts[$i]->id;
      $name_of_the_district = $randLayouts[$i]->name_of_the_district;
      $name_of_the_mandal = $randLayouts[$i]->name_of_the_mandal;
      $name_of_the_village = $randLayouts[$i]->name_of_the_village;
      $allotted_block = $randLayouts[$i]->allotted_block;
      $id = $i+1;
      $updateAptAlt = DB::update("update aptalt set field1 = '$plotAssign', field2 = '$plotAssignId', name_of_the_district = '$name_of_the_district', name_of_the_mandal = '$name_of_the_mandal', name_of_the_village = '$name_of_the_village', allotted_block = '$allotted_block' where id = ?", [$id]);
      // return $updateAptAlt;
    }
    return redirect('/block/success');
    return 'Success';
    $perFloor = 12;
    $members = count($list);
    // return $members;
    $apts = ceil($members / 48); // Number of Appartments
    // return $apts;
    $c = round($apts * 12);
    // return $c;
    $g = round($apts * 12);
    $fr = (round($apts * 12)) + $g;
    // return $f;
    $s = (round($apts * 12)) + $fr;
    $t = (round($apts * 12)) + $s;
    // return $f;
    // return count($updateList);
    $y = 4;

    for ($i = 0; $i < $apts; $i++) { // 18 times for 18 Appartments
      // $membersperfloor = $apts * 8;
      // return $membersperfloor;
      // $gd = 0;
      $cc = $c - $apts; //198
      $floorCount = 1;
      for ($f = 0; $f < $y; $f++) { // 4 times for 4 flores
        // echo "$f";
        if ($f == 1) {
          $flr = 'G';
          $groundFloor = DB::SELECT("SELECT * FROM calc ORDER BY days DESC limit 0, $c"); // Ground Floor
          // return count($groundFloor);
          $appp = 1;
          $app = 1;
          // return $gd;
          for ($gd = 0; $gd < $c; $gd += $perFloor) { // 18 times
            // if ($app == 8) {
            //   $appp += 1;
            // }
            $array = array_slice($groundFloor, $gd, 12, true);
            // return count($array);

            $fcount = 1;
            foreach ($array as $key) {
              // var_dump($key->name);


              $updateId = $key->id;
              $category = $key->category;
              if ($category == 1) {
                $cat = "A";
              } elseif ($category == 2) {
                $cat = "B";
              } elseif ($category == 3) {
                $cat = "C";
              }
              $updt = $cat . ',' . $appp . ',' . $flr . ',' . $fcount;
              $blockno = $cat . $appp;
              $floorno = $flr;
              $flatno = $fcount;
              $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
              $app++;
              $fcount++;
            }
            $app = 1;
            $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 5;
            }
            if ($appp == 6) {
              $appp = 7;
            }
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 13) {
              $appp = 14;
            }
            if ($appp == 15) {
              $appp = 16;
            }
            if ($appp == 17) {
              $appp = 18;
            }
            if ($appp == 19) {
              $appp = 20;
            }
            if ($appp == 21) {
              $appp = 22;
            }
            if ($appp == 23) {
              $appp = 24;
            }
            if ($appp == 25) {
              $appp = 26;
            }
            if ($appp == 29) {
              $appp = 30;
            }
            if ($appp == 31) {
              $appp = 32;
            }
            if ($appp == 33) {
              $appp = 34;
            }
            if ($appp == 35) {
              $appp = 36;
            }
            if ($appp == 37) {
              $appp = 38;
            }
            if ($appp == 39) {
              $appp = 40;
            }
            if ($appp == 41) {
              $appp = 42;
            }
            if ($appp == 43) {
              $appp = 44;
            }
            if ($appp == 47) {
              $appp = 48;
            }
          }
        } elseif ($f == 2) {
          $flr = 'F';
          // echo "string";
          // return $c;
          $firstFloor = DB::SELECT("SELECT * FROM calc ORDER BY days DESC limit $g, $c"); // First Floor
          // return $firstFloor;
          $appp = 1;
          $app = 1;
          for ($gd = 0; $gd < $c; $gd += $perFloor) {
            // if ($app == 8) {
            //   $appp += 1;
            // }
            $arrayF = array_slice($firstFloor, $gd, 12, true);

            $fcount = 1;
            foreach ($arrayF as $key) {
              // var_dump($key->name);


              $updateId = $key->id;
              $category = $key->category;
              if ($category == 1) {
                $cat = "A";
              } elseif ($category == 2) {
                $cat = "B";
              } elseif ($category == 3) {
                $cat = "C";
              }
              // $updt = $cat.$appp.$flr.$fcount;
              $updt = $cat . ',' . $appp . ',' . $flr . ',' . $fcount;
              // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
              $blockno = $cat . $appp;
              $floorno = $flr;
              $flatno = $fcount;
              $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
              $app++;
              $fcount++;
            }
            $app = 1;
            $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 5;
            }
            if ($appp == 6) {
              $appp = 7;
            }
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 13) {
              $appp = 14;
            }
            if ($appp == 15) {
              $appp = 16;
            }
            if ($appp == 17) {
              $appp = 18;
            }
            if ($appp == 19) {
              $appp = 20;
            }
            if ($appp == 21) {
              $appp = 22;
            }
            if ($appp == 23) {
              $appp = 24;
            }
            if ($appp == 25) {
              $appp = 26;
            }
            if ($appp == 29) {
              $appp = 30;
            }
            if ($appp == 31) {
              $appp = 32;
            }
            if ($appp == 33) {
              $appp = 34;
            }
            if ($appp == 35) {
              $appp = 36;
            }
            if ($appp == 37) {
              $appp = 38;
            }
            if ($appp == 39) {
              $appp = 40;
            }
            if ($appp == 41) {
              $appp = 42;
            }
            if ($appp == 43) {
              $appp = 44;
            }
            if ($appp == 47) {
              $appp = 48;
            }
          }
        } elseif ($f == 3) {
          $flr = 'S';
          // DB::connection()->enableQueryLog();
          // return $c;
          // return 'iiii';
          $secondFloor = DB::SELECT("SELECT * FROM calc ORDER BY days DESC limit $fr, $c"); // Ground Floor
          // dd(DB::getQueryLog());
          // return $secondFloor;
          $appp = 1;
          $app = 1;
          for ($gd = 0; $gd < $c; $gd += $perFloor) {
            // if ($app == 8) {
            //   $appp += 1;
            // }
            $arrayS = array_slice($secondFloor, $gd, 12, true);
            // return $arrayS;
            $fcount = 1;
            foreach ($arrayS as $key) {
              // var_dump($key->name);


              $updateId = $key->id;
              $category = $key->category;
              if ($category == 1) {
                $cat = "A";
              } elseif ($category == 2) {
                $cat = "B";
              } elseif ($category == 3) {
                $cat = "C";
              }
              // $updt = $cat.$appp.$flr.$fcount;
              $updt = $cat . ',' . $appp . ',' . $flr . ',' . $fcount;
              // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
              $blockno = $cat . $appp;
              $floorno = $flr;
              $flatno = $fcount;
              $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
              $app++;
              $fcount++;
            }
            $app = 1;
            $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 5;
            }
            if ($appp == 6) {
              $appp = 7;
            }
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 13) {
              $appp = 14;
            }
            if ($appp == 15) {
              $appp = 16;
            }
            if ($appp == 17) {
              $appp = 18;
            }
            if ($appp == 19) {
              $appp = 20;
            }
            if ($appp == 21) {
              $appp = 22;
            }
            if ($appp == 23) {
              $appp = 24;
            }
            if ($appp == 25) {
              $appp = 26;
            }
            if ($appp == 29) {
              $appp = 30;
            }
            if ($appp == 31) {
              $appp = 32;
            }
            if ($appp == 33) {
              $appp = 34;
            }
            if ($appp == 35) {
              $appp = 36;
            }
            if ($appp == 37) {
              $appp = 38;
            }
            if ($appp == 39) {
              $appp = 40;
            }
            if ($appp == 41) {
              $appp = 42;
            }
            if ($appp == 43) {
              $appp = 44;
            }
            if ($appp == 47) {
              $appp = 48;
            }
          }
        } else {
          // return 'lllll';
          $flr = 'T';
          // DB::connection()->enableQueryLog();
          $thirdFloor = DB::SELECT("SELECT * FROM calc ORDER BY days DESC limit $s, $c"); // Ground Floor
          // dd(DB::getQueryLog());
          // return $secondFloor;
          $appp = 1;
          $app = 1;
          for ($gd = 0; $gd < $c; $gd += $perFloor) {
            // if ($app == 8) {
            //   $appp += 1;
            // }
            $arrayT = array_slice($thirdFloor, $gd, 12, true);
            // return $arrayS;
            $fcount = 1;
            foreach ($arrayT as $key) {
              // var_dump($key->name);


              $updateId = $key->id;
              $category = $key->category;
              if ($category == 1) {
                $cat = "A";
              } elseif ($category == 2) {
                $cat = "B";
              } elseif ($category == 3) {
                $cat = "C";
              }
              // $updt = $cat.$appp.$flr.$fcount;
              $updt = $cat . ',' . $appp . ',' . $flr . ',' . $fcount;
              // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
              $blockno = $cat . $appp;
              $floorno = $flr;
              $flatno = $fcount;
              $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
              $app++;
              $fcount++;
            }
            $app = 1;
            $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 5;
            }
            if ($appp == 6) {
              $appp = 7;
            }
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 13) {
              $appp = 14;
            }
            if ($appp == 15) {
              $appp = 16;
            }
            if ($appp == 17) {
              $appp = 18;
            }
            if ($appp == 19) {
              $appp = 20;
            }
            if ($appp == 21) {
              $appp = 22;
            }
            if ($appp == 23) {
              $appp = 24;
            }
            if ($appp == 25) {
              $appp = 26;
            }
            if ($appp == 29) {
              $appp = 30;
            }
            if ($appp == 31) {
              $appp = 32;
            }
            if ($appp == 33) {
              $appp = 34;
            }
            if ($appp == 35) {
              $appp = 36;
            }
            if ($appp == 37) {
              $appp = 38;
            }
            if ($appp == 39) {
              $appp = 40;
            }
            if ($appp == 41) {
              $appp = 42;
            }
            if ($appp == 43) {
              $appp = 44;
            }
            if ($appp == 47) {
              $appp = 48;
            }
          }
        }
      }
    }
    $below50 = DB::SELECT("SELECT * FROM calc WHERE days <= 49 ORDER BY days DESC"); // Ground Floor
    $sf = [];
    foreach ($below50 as $b50) {
      array_push($sf, $b50->allotment);
    }
    $array = $sf;
    shuffle($array);
    $updateallot = DB::SELECT("UPDATE calc set allotment = null WHERE days <= 49");
    foreach ($array as $key) { // A9G2
      $below50up = DB::UPDATE("UPDATE `calc` set allotment = '$key' WHERE allotment IS null ORDER BY RAND() LIMIT 1"); // Ground Floor
    }
    return redirect('/block/a');
    }






    // Genarate B Block
    public function generateb(Request $request)
    {
      $list = DB::SELECT("SELECT * FROM calcb ORDER BY days DESC");
      // return $list;
      $perFloor = 12;
      $members = count($list);
      // return $members;
      $apts = ceil($members / 48); // Number of Appartments
      // return $apts;
      $c = round($apts * 12);
      // return $c;
      $g = round($apts * 12);
      $fr = (round($apts * 12)) + $g;
      // return $f;
      $s = (round($apts * 12)) + $fr;
      $t = (round($apts * 12)) + $s;
      // return $f;
      // return count($updateList);
      $y = 4;

      for ($i=0; $i < $apts; $i++) { // 18 times for 18 Appartments
        // $membersperfloor = $apts * 8;
        // return $membersperfloor;
        // $gd = 0;
        $cc= $c - $apts; //198
        $floorCount = 1;
        for ($f=0; $f < $y; $f++) { // 4 times for 4 flores
          // echo "$f";
          if ($f == 1) {
            $flr = 'G';
            $groundFloor = DB::SELECT("SELECT * FROM calcb ORDER BY days DESC limit 0, $c"); // Ground Floor
            // return count($groundFloor);
            $appp = 7;
            $app = 1;
            // return $gd;
            for ($gd=0; $gd < $c; $gd+=$perFloor) { // 18 times
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $array = array_slice($groundFloor, $gd, 12, true);
              // return count($array);

              $fcount = 1;
              foreach ($array as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcb` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            }
          } elseif ($f == 2) {
            $flr = 'F';
            // echo "string";
            // return $c;
            $firstFloor = DB::SELECT("SELECT * FROM calcb ORDER BY days DESC limit $g, $c"); // First Floor
            // return $firstFloor;
            $appp = 7;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayF = array_slice($firstFloor, $gd, 12, true);

              $fcount = 1;
              foreach ($arrayF as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcb` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            }
          } elseif ($f == 3) {
            $flr = 'S';
            // DB::connection()->enableQueryLog();
            // return $c;
            // return 'iiii';
            $secondFloor = DB::SELECT("SELECT * FROM calcb ORDER BY days DESC limit $fr, $c"); // Ground Floor
            // dd(DB::getQueryLog());
            // return $secondFloor;
            $appp = 7;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayS = array_slice($secondFloor, $gd, 12, true);
              // return $arrayS;
              $fcount = 1;
              foreach ($arrayS as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcb` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            }
          } else {
            // return 'lllll';
            $flr = 'T';
            // DB::connection()->enableQueryLog();
            $thirdFloor = DB::SELECT("SELECT * FROM calcb ORDER BY days DESC limit $s, $c"); // Ground Floor
            // dd(DB::getQueryLog());
            // return $secondFloor;
            $appp = 7;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayT = array_slice($thirdFloor, $gd, 12, true);
              // return $arrayS;
              $fcount = 1;
              foreach ($arrayT as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcb` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 8) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            }
          }

        }

      }
      $below50 = DB::SELECT("SELECT * FROM calcb WHERE days <= 49 ORDER BY days DESC"); // Ground Floor
      $sf = [];
      foreach ($below50 as $b50) {
        array_push($sf, $b50->allotment);
      }
      $array = $sf;
      shuffle($array);
      // return $array;
      // return $shuffle;
      // return $below50up;
      $updateallot = DB::SELECT("UPDATE calcb set allotment = null WHERE days <= 49");
      foreach ($array as $key) { // A9G2
        $below50up = DB::UPDATE("UPDATE `calcb` set allotment = '$key' WHERE allotment IS null ORDER BY RAND() LIMIT 1"); // Ground Floor
        // print_r($key);
        // echo "<pre>";
        // return $key;
        // foreach ($below50up as $b50up) {
        //   $updateId = $b50up->id;
        //   $update = DB::UPDATE("UPDATE `calc` set allotment = '$key' WHERE id = $updateId");
        // }
        // return $key;
      }
      // $sms = DB::SELECT("SELECT * FROM calcb"); // Ground Floor
      // foreach ($sms as $s) {
      //   if ($s->mobile != '') {
      //
      //     $flatDetails = $s->allotment;
      //     $exp = explode(',', $flatDetails);
      //     $blockno = $exp[0].$exp[1];
      //     $flatno = $exp[2].$exp[3];
      //     $mobile = $s->mobile;
      //     $message = "Dear $s->name of the application you have been allotted block no $blockno. flat no $flatno floor in pmay ntr urban housing in punganur Municipality ...  By commissioner punganur municipality";
      //     $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=COMPGR&password=abc@123&to={$mobile}&from=COMPGR&message=".urlencode($message);
      //     file($url);
      //   }
      //
      // }
      // return $array;
      return redirect('/block/b');
    }




    // Generate C Block
    public function generatec(Request $request)
    {

      // $below50up = DB::SELECT("SELECT * FROM calc WHERE days <= 49 AND allotment IS null ORDER BY RAND()"); // Ground Floor
      // return $below50up;
      // $my_array = array("red","green","blue","yellow","purple");
      // shuffle($my_array);
      // print_r($my_array);
      // return $my_array;

      // $table = '';
      // if (condition) {
      //   // code...
      // }

      $list = DB::SELECT("SELECT * FROM calcc ORDER BY days DESC");
      // return $list;
      $perFloor = 12;
      $members = count($list);
      $apts = ceil($members / 48); // Number of Appartments
      // return $apts;
      $c = round($apts * 12);
      // return $c;
      $g = round($apts * 12);
      $fr = (round($apts * 12)) + $g;
      // return $f;
      $s = (round($apts * 12)) + $fr;
      $t = (round($apts * 12)) + $s;
      // return $f;
      // return count($updateList);
      $y = 4;

      // return $groundFloor;
      // echo "string";

      // $array = array_slice($groundFloor, 8, 8, true);
      // foreach ($array as $key) {
      //   var_dump($key->name);
      // }
      for ($i=0; $i < $apts; $i++) { // 18 times for 18 Appartments
        // $membersperfloor = $apts * 8;
        // return $membersperfloor;
        // $gd = 0;
        $cc= $c - $apts; //198
        $floorCount = 1;
        for ($f=0; $f < $y; $f++) { // 4 times for 4 flores
          // echo "$f";
          if ($f == 1) {
            $flr = 'G';
            $groundFloor = DB::SELECT("SELECT * FROM calcc ORDER BY days DESC limit 0, $c"); // Ground Floor
            // return count($groundFloor);
            $appp = 1;
            $app = 1;
            // return $gd;
            for ($gd=0; $gd < $c; $gd+=$perFloor) { // 18 times
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $array = array_slice($groundFloor, $gd, 12, true);
              // return count($array);

              $fcount = 1;
              foreach ($array as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            if ($appp == 14) {
              $appp = 18;
            }
            }
          } elseif ($f == 2) {
            $flr = 'F';
            // echo "string";
            // return $c;
            $firstFloor = DB::SELECT("SELECT * FROM calcc ORDER BY days DESC limit $g, $c"); // First Floor
            // return $firstFloor;
            $appp = 1;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayF = array_slice($firstFloor, $gd, 12, true);

              $fcount = 1;
              foreach ($arrayF as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            if ($appp == 14) {
              $appp = 18;
            }
            }
          } elseif ($f == 3) {
            $flr = 'S';
            // DB::connection()->enableQueryLog();
            // return $c;
            // return 'iiii';
            $secondFloor = DB::SELECT("SELECT * FROM calcc ORDER BY days DESC limit $fr, $c"); // Ground Floor
            // dd(DB::getQueryLog());
            // return $secondFloor;
            $appp = 1;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayS = array_slice($secondFloor, $gd, 12, true);
              // return $arrayS;
              $fcount = 1;
              foreach ($arrayS as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            if ($appp == 14) {
              $appp = 18;
            }
            }
          } else {
            // return 'lllll';
            $flr = 'T';
            // DB::connection()->enableQueryLog();
            $thirdFloor = DB::SELECT("SELECT * FROM calcc ORDER BY days DESC limit $s, $c"); // Ground Floor
            // dd(DB::getQueryLog());
            // return $secondFloor;
            $appp = 1;
            $app = 1;
            for ($gd=0; $gd < $c; $gd+=$perFloor) {
              // if ($app == 8) {
              //   $appp += 1;
              // }
              $arrayT = array_slice($thirdFloor, $gd, 12, true);
              // return $arrayS;
              $fcount = 1;
              foreach ($arrayT as $key) {
                // var_dump($key->name);


                    $updateId = $key->id;
                    $category = $key->category;
                    if ($category == 1) {
                      $cat = "A";
                    } elseif ($category == 2) {
                      $cat = "B";
                    } elseif ($category == 3) {
                      $cat = "C";
                    }
                    // $updt = $cat.$appp.$flr.$fcount;
                    $updt = $cat.','.$appp.','.$flr.','.$fcount;
                    // $update = DB::UPDATE("UPDATE `calc` set allotment = '$updt' WHERE id = $updateId");
                    $blockno = $cat.$appp;
                    $floorno = $flr;
                    $flatno = $fcount;
                    $update = DB::UPDATE("UPDATE `calcc` set allotment = '$updt', blockno = '$blockno', floorno = '$floorno', flatno = '$flatno' WHERE id = $updateId");
                $app++;
                $fcount++;
              }
              $app = 1;
              $appp += 1;
            if ($appp == 2) {
              $appp = 3;
            }
            if ($appp == 4) {
              $appp = 9;
            }
            if ($appp == 10) {
              $appp = 11;
            }
            if ($appp == 12) {
              $appp = 13;
            }
            if ($appp == 14) {
              $appp = 18;
            }
            }
          }

        }

      }
      $below50 = DB::SELECT("SELECT * FROM calcc WHERE days <= 49 ORDER BY days DESC"); // Ground Floor
      $sf = [];
      foreach ($below50 as $b50) {
        array_push($sf, $b50->allotment);
      }
      $array = $sf;
      shuffle($array);
      // return $array;
      // return $shuffle;
      // return $below50up;
      $updateallot = DB::SELECT("UPDATE calcc set allotment = null WHERE days <= 49");
      foreach ($array as $key) { // A9G2
        $below50up = DB::UPDATE("UPDATE `calcc` set allotment = '$key' WHERE allotment IS null ORDER BY RAND() LIMIT 1"); // Ground Floor
        // print_r($key);
        // echo "<pre>";
        // return $key;
        // foreach ($below50up as $b50up) {
        //   $updateId = $b50up->id;
        //   $update = DB::UPDATE("UPDATE `calc` set allotment = '$key' WHERE id = $updateId");
        // }
        // return $key;
      }
      // $sms = DB::SELECT("SELECT * FROM calcc"); // Ground Floor
      // foreach ($sms as $s) {
      //   if ($s->mobile != '') {
      //
      //     $flatDetails = $s->allotment;
      //     $exp = explode(',', $flatDetails);
      //     $blockno = $exp[0].$exp[1];
      //     $flatno = $exp[2].$exp[3];
      //     $mobile = $s->mobile;
      //     $message = "Dear $s->name of the application you have been allotted block no $blockno. flat no $flatno floor in pmay ntr urban housing in punganur Municipality ...  By commissioner punganur municipality";
      //     $url = "http://onlinebulksmslogin.com/spanelv2/api.php?username=COMPGR&password=abc@123&to={$mobile}&from=COMPGR&message=".urlencode($message);
      //     file($url);
      //   }
      //
      // }
      // return $array;
      return redirect('/block/c');
    }

    public function pdf($allotment)
    {
      return view('pdf')->with('allotment', $allotment);
    }

    public function ApplicationUpdate($id)
    {
      // return $id;
      $id_check = Application::find($id);
      return view('applicationUpdate')->with('key', $id_check);
    }
    public function StatusUpdate(Request $request)
    {
      // return $request;
      $id_update = Application::find($request->id);
      $id_update->applicantName = $request->applicantName;
      $id_update->adharNumber = $request->adharNumber;
      $id_update->mobileNo = $request->mobileNo;
      $id_update->houseNo = $request->houseNo;
      $id_update->village = $request->village;
      $id_update->surveyNo = $request->surveyNo;
      $id_update->extentAsPerApplication = $request->extentAsPerApplication;
      $id_update->extentTotalConfirmedAfterEnquiryOfSurveyor = $request->extentTotalConfirmedAfterEnquiryOfSurveyor;
      $id_update->rationCardNumber = $request->rationCardNumber;
      $id_update->sts = $request->sts;
      $id_update->save();
      return redirect('/mro');
    }
}
